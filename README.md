# Projeto residência UNIT 2020.2
# Desafio CESAR

# Squad marco[0]
- Arthur de Oliveira Ávila
- Auriclecio Marques Pereira
- Bárbara Maria de Souza Brito
- Bruno Pessoa da Silva Malta
- Fábio José da Silva
- Gilberto Luiz de Souza Lopes Júnior
- José Alberto Sales Júnior
- Júlio Alves Jaruzo dos Santos
- Marcelo José Soares Silva Filho
- Tarcísio Silva Lacerda Ribeiro

# Ambiente

- Instalar o git (https://git-scm.com/downloads)
- Instalar o Node.js (https://nodejs.org/pt-br/download/)

# Repositório

https://gitlab.com/gilbertoljunior/projeto-residencia-cesar

# Configurar aplicação

$ npm install

# Inicia a aplicação

$ npm start

# Acesso aplicação via browser

http://localhost:5000/



