var express = require('express')
var consign = require('consign')
var bodyParser = require('body-parser')
var multer = require('multer')
var csv = require('csvtojson')
var fs = require('fs')
var https = require('https')


var app = express()

app.use(bodyParser.urlencoded({ extended: true }))


app.get('/upload', function(req, res) {
    const url = 'https://docs.google.com/uc?export=download&id=17f2ip_IHB_iABdvNAacheYkLhDxRXKJm'

    https.get(url, function(res) {

        const fileStream = fs.createWriteStream('uploads/data.csv');
        var dataUrl = res.headers.location
        https.get(dataUrl, function(resUrl) {
            resUrl.pipe(fileStream);


            csv()
                .fromFile('uploads/data.csv')
                .then((jsonObj) => {


                    const data = JSON.stringify(jsonObj);
                    fs.writeFile('arquivo.json', data, (err) => {
                        if (err) throw err;
                        console.log('O arquivo foi criado!');
                    });
                })
        })

    });



    res.redirect('confirmUpdate')
})



app.set('view engine', 'ejs')
app.set('views', './views')
app.use(express.static('./public'))

consign() // Carrega o módulo 'consign'
    .include('src/routes') // Inclui o caminho da pasta 'routes'
    .then('src/controllers') // Indica o caminho dos arquivos de 'controllers'
    .then('src/models') // Indica o caminho dos arquivos de 'models'
    .into(app) // Injeta na variável 'app' todas as funções do 'consign'


module.exports = app // Chama a função 'app'